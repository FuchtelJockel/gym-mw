"use strict";

function request(url, type, callback) {
  var request = new XMLHttpRequest();
  request.responseType = type;
  request.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      callback(request.response);
    }
  }
  request.open('GET', url, true);
  request.send();
}

request('/gym-mw/images/gym_mw.svg', 'text', function (content) {
  document.getElementById("gym_mw_svg").innerHTML = content;
});

request('/gym-mw/images/logo.svg', 'text', function (content) {
  document.getElementById("logo_svg").innerHTML = content;
});
