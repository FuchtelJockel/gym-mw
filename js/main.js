"use strict";
var side_menu_state = '';

main();

function main() {
  var top_menu = document.getElementById('nav_bar');
  request('/gym-mw/js/json/top_menu.json', 'json', function(json) {
    buildMenu(top_menu, json);
    addListeners(top_menu);
  });
  menu_button.addEventListener('click', function() {
    sideMenuAnim();
    side_menu_state = 'closed';
  },false);
}

function clearDiv(element) {
  while (element.firstChild) {
    element.removeChild(element.firstChild);
  }
}

function createSubList(json) {
  let ul_node = document.createElement('UL');
  for (var key in json) {
    if (json.hasOwnProperty(key)) {
      let li_node = document.createElement('LI');
      if (typeof(json[key]) == 'object') {
        li_node.innerHTML = key + ':';
        li_node.appendChild(createSubList(json[key]));
      } else {
        let a_node = document.createElement('A');
        a_node.innerHTML = key;
        a_node.dataset.link = json[key];
        li_node.appendChild(a_node);
      }
      ul_node.appendChild(li_node);
    }
  }
  ul_node.classList.add('sub_menu');
  return ul_node;
}

function buildMenu(element, json) {
  clearDiv(element);
  createList(element, json);
  function createList(element, json) {
    for (var key in json) {
      if (json.hasOwnProperty(key)) {
        let li_node = document.createElement('LI');
        if (typeof(json[key]) == 'object') {
          li_node.innerHTML = key + ':';
          li_node.appendChild(createSubList(json[key]));
        }else {
          let a_node = document.createElement('A');
          a_node.innerHTML = key;
          a_node.dataset.link = json[key];
          li_node.appendChild(a_node);
        }
        element.appendChild(li_node);
      }
    }
  }
}

function addListeners(element) {
  let el_nodes = element.childNodes;
  el_nodes.forEach(function(element) {
    if (element.nodeName == 'LI') {
      element.addEventListener('click', function(event) {
        if (element.parentNode.id == 'nav_bar') {
          navButton(event);
        } else if (element.parentNode.id == 'side_menu') {
          menuButton(event);
        }
      }, false);
    }
  });
}

function navButton(event) {
  let element = event.target;
  let side_menu = document.getElementById('side_menu');
  request(element.dataset.link , 'json', function(json) {
    buildMenu(side_menu, json);
    addListeners(side_menu);
  });
  if ( side_menu_state == element ) {
    sideMenuAnim();
  }else  if ( side_menu_state == 'closed' ) {
    sideMenuAnim();
  }
  side_menu_state = element;
}

function menuButton(event) {
  let element = event.target;
  let container = document.getElementById('container');
  console.log(element);
  clearDiv(container);
  let url = '/gym-mw/html/' + element.dataset.link;
  request(url, 'text', function(text) {
    let center_div = document.createElement('DIV');
    let content_div = document.createElement('DIV');
    center_div.classList.add('center');
    content_div.classList.add('content')
    content_div.innerHTML = text;
    center_div.appendChild(content_div);
    container.appendChild(center_div);
  });
}

function sideMenuAnim() {
  let menu = document.getElementById('menu');
  let menu_button = document.getElementById('menu_button');
  let menu_button_sym_anim = document.getElementById('menu_button_sym');
  menu.classList.toggle('menu_anim');
  menu_button.classList.toggle('button_anim');
  menu_button.classList.add('fade_in_anim');
  menu_button_sym_anim.classList.toggle('menu_button_sym_anim');
}
